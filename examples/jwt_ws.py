import requests
import json
from websocket import create_connection

data = {"email": "norm@norm.com", "password": "daviddavid1"}
headers = {"Content-Type": "application/json"}
r = requests.post("http://192.168.99.100:8000/api/auth/token/", data=json.dumps(data), headers=headers)

token = r.json()["token"]

headers = {"authorization": f"JWT {token}"}

ws = create_connection("ws://192.168.99.100:8000/ws/monitor/", header=headers)

print("Sending")
ws.send('{"type": "ping"}')
print("Sent")
print("Receiving...")
result = ws.recv()
print(f"Received {result}")
ws.close()
