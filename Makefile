build:
	docker-compose build

up:
	docker-compose up -d

up-non-daemon:
	docker-compose up

start:
	docker-compose start

stop:
	docker-compose stop

test:
	docker-compose exec web python manage.py test

restart:
	docker-compose stop && docker-compose start

restart-web:
	docker-compose restart web

shell-nginx:
	docker exec -ti nz01 bash

shell-web:
	docker exec -ti dz01 bash

shell-db:
	docker exec -ti pz01 bash

log-nginx:
	docker-compose logs nginx

log-web:
	docker-compose logs web

log-db:
	docker-compose logs db

collectstatic:
	docker exec dz01 /bin/sh -c "python manage.py collectstatic --noinput"
