# ECMM427-web

The web server which acts as a central command and control system for the project.

## Technology stack

1. Docker - used for ease of cross-platform development
2. Python - The language underpinning the majority of our project
3. Django - Web framework
4. Daphne - Web server that manages our Django ecosystem
5. Nginx - Web server/proxy which hosts static content and proxies requests to Daphne
6. Postgres - Powerful SQL database
7. Celery - Allows tasks to be run asynchronously or at set intervals
8. Redis - Used as a message broker for Celery

## Basic Usage

In a development environment, make sure you have both `docker` and `docker-compose` installed and available within your shell.

To setup the project:

```
make build
make up
```

To start and stop the containers:

```
make start
make stop
```

More functionality can be seen within the `Makefile`.

# OSX  specific

Installation with `brew`:

```
brew install docker docker-machine docker-compose
brew cask install virtualbox
docker-machine create --driver virtualbox default
docker-machine start default
eval "$(docker-machine env default)"
```

Getting network addresses:

```
docker-machine ip default
```

# Odds and ends

To create a superuser:

```
docker exec -it dz01 /bin/sh -c "python manage.py createsuperuser"  
```

# Setting up development environment

You will always be on a different address in development. For security, we must 'whitelist' that domain within settings. Do so by simply adding it to the list under ```ALLOWED_HOSTS``` and restart using ```make restart```.


## Notes

Changed redis from 6379 to 6380, was clashing with host system.
