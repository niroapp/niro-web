from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

import accounts.views as accounts_views
import warehouse.views as warehouse_views
import ecmm427.views as ecmm427_views


router = routers.DefaultRouter()
router.register(r'users', accounts_views.UserViewSet)
router.register(r'groups', accounts_views.GroupViewSet)
router.register(r'samples', warehouse_views.SampleViewSet, 'Sample')
router.register(r'records', warehouse_views.RecordViewSet, 'Record')


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/auth/', include('rest_framework.urls')),
    url(r'^api/auth/token/', obtain_jwt_token),
    url(r'^api/auth/token/refresh/', refresh_jwt_token),
    url(r'^api/', include(router.urls)),
    url(r'^$', ecmm427_views.Homepage.as_view()),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
