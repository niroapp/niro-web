from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.files.base import ContentFile

from channels.consumer import SyncConsumer
from channels.generic.websocket import JsonWebsocketConsumer

from asgiref.sync import async_to_sync


from .models import Predictor, Sample, Record

from .utils.predictor import PredictorSystem

import json


class MonitorConsumer(JsonWebsocketConsumer):
    def connect(self):
        if "user" not in self.scope:
            return

        self.user = self.scope["user"]

        async_to_sync(self.channel_layer.group_add)("monitors", self.channel_name)

        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)("monitors", self.channel_name)

    def message(self, event):
        # Only send notifications that are relevant to the user
        if Sample.objects.filter(owner=self.user, id=event["sample_id"]).exists():
            self.send_json(event["results"])

    def receive_json(self, event):
        if event["type"] == "ping":
            self.send_json({"type": "pong"})


class AnalysisConsumer(SyncConsumer):
    def message(self, event):
        predictor = Predictor.objects.get(id=event["predictor_id"])

        with predictor.moiety_names.open(mode='r') as f:
            moiety_names = json.load(f)

        with predictor.compounds_mapping.open(mode='r') as f:
            compounds_mapping = json.load(f)

        with predictor.training_data.open(mode='r') as f:
            training_data = json.load(f)

        predictor_system = PredictorSystem(moiety_names, compounds_mapping, training_data)

        sample = Sample.objects.get(id=event["sample_id"])
        records = Record.objects.filter(sample=sample)

        averaged_read = [0] * 331
        for record in records:
            for i in range(len(averaged_read)):
                averaged_read[i] += record.spectrum[i]

        for i in range(len(averaged_read)):
            averaged_read[i] /= records.count()

        popt = predictor_system.predict(averaged_read)

        sample.analysis = predictor_system.label(popt)

        f = predictor_system.plot(averaged_read, popt)
        content_file = ContentFile(f.getvalue())
        sample.spectrum.save('spectrum.png', content_file)

        sample.save()

        result = {
            "reference": sample.reference,
            "name": sample.name,
            "spectrum": sample.spectrum.url,
            "type": sample.analysis
        }

        async_to_sync(self.channel_layer.group_send)(
            "monitors",
            {
                "type": "message",
                "sample_id": sample.id,
                "results": result,
            }
        )
