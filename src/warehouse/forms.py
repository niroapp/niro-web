from django import forms


class UpdateModelForm(forms.Form):
    moieties_spreadsheet = forms.FileField()
    collection_json = forms.FileField()
