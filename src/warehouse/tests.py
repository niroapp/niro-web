from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile

from .models import Predictor, Sample, Record
from .utils.predictor import PredictorSystem

import json
import os
import random


class PredictorTestCase(TestCase):
    def setUp(self):
        self.moiety_names = ["a", "b"]
        self.compounds_mapping = {
            "water": {"rmm": 18.0, "density": 1000.0, "values": {"a": 2.0}},
            "ethanol": {"rmm": 18.0, "density": 1000.0, "values": {"b": 2.0}},
        }

        self.training_data = [
            {"water": 1.0, "volumes": {"ethanol": 1.0}, "masses": {}, "spectra": [1.0] * 311},
            {"water": 1.0, "volumes": {"ethanol": 2.0}, "masses": {}, "spectra": [2.0] * 311},
            {"water": 1.0, "volumes": {"ethanol": 3.0}, "masses": {}, "spectra": [3.0] * 311},
        ]

    def test_predictor_system_create(self):
        PredictorSystem(self.moiety_names, self.compounds_mapping, self.training_data)

    def test_predictor_model_save(self):
        # Files must be deleted, so test occours within a try/except clause
        exception = None

        try:
            predictor = Predictor.objects.create(name="Test")
            predictor.moiety_names = SimpleUploadedFile("moity_names.json", json.dumps(self.moiety_names).encode())
            predictor.compounds_mapping = SimpleUploadedFile("compounds_mapping.json", json.dumps(self.compounds_mapping).encode())
            predictor.training_data = SimpleUploadedFile("training_data.json", json.dumps(self.training_data).encode())
            predictor.save()
        except Exception as e:
            exception = e

        os.remove(predictor.moiety_names.path)
        os.remove(predictor.compounds_mapping.path)
        os.remove(predictor.training_data.path)

        # An exception cannow be raised
        if exception is not None:
            raise exception


class PredictorIntegrationTestCase(TestCase):
    def setUp(self):
        self.moiety_names = ["HOH", "CH3"]
        self.compounds_mapping = {
            "water": {"rmm": 18.0, "density": 1000.0, "values": {"HOH": 2.0}},
            "ethanol": {"rmm": 18.0, "density": 1000.0, "values": {"CH3": 2.0}},
        }

        self.training_data = []

        for x in range(50):
            self.training_data.append({"water": 0.5, "volumes": {"ethanol": (x + random.random()) / 100.0}, "masses": {}, "spectra": [x + random.random() for i in range(311)]})

    def test_predict(self):
        predictor_system = PredictorSystem(self.moiety_names, self.compounds_mapping, self.training_data)

        self.assertEqual(len(predictor_system.predict([0.2] * 311)), 2)

    def test_label(self):
        predictor_system = PredictorSystem(self.moiety_names, self.compounds_mapping, self.training_data)

        popt = predictor_system.predict([0.2] * 311)
        prediction = predictor_system.label(popt)
        self.assertEqual(len(prediction["substances"]), 2)
