import matplotlib.pyplot as plt
from matplotlib import cm
import json
import numpy as np
from scipy import stats, optimize
import copy
import random
import os
import io

class PredictorSystem:
    def __init__(self, moiety_names, compounds_mapping, training_data):
        self.moiety_names = moiety_names
        self.compounds_mapping = compounds_mapping
        self.training_data = training_data

        # Hardcoded from experiments
        self.compound_names = [
            'water', 'ethanol', 'acetone', 'citronellol', 'dipentene', 'carvone',
            'toluene', 'linalool', 'diisobutyl-ketone', 'tridec-1-ene', 'geraniol',
            'eugenol', 'methanol', 'nerolidol', 'heptanal', 'cyclohexanethiol', 'diphenyl-ether',
            'acetonitrile', 'methyl-acetate', 'cyclohexanone-diethylketal', '2-butyne',
            'ethylene-glycol', 'benzaldehyde', 'i-pentyl-acetate', 'cyclohexeneoxide',
            '1,4-cyclohexadiene', 'propane-1-2-diol', 'furan', 't-butanol', 't-butyl-methyl-ether',
            'alpha-angelica-lactone'
        ]

        self.process_training_data()
        self.generate_model()

    def process_training_data(self):
        self.inputs = []
        self.targets = []

        for record in self.training_data:
            found = False
            for compound_name in self.compound_names:
                if compound_name in record['volumes']:
                    found = True

            if not found:
                continue

            # We're only looking at liquids in water
            if record['water'] == 0 and ("HOH" not in record['volumes'] or record['volumes']["HOH"] == 0):
                continue

            # Ignore huge valued records
            if record['water'] > 1 or [x for x in record["volumes"] if record["volumes"][x] > 1]:
                continue

            target = [0] * len(self.compound_names)

            # Given equation for the water value
            index = self.compound_names.index("water")
            target[index] += float(record['water'])

            for compound in record['volumes']:
                index = self.compound_names.index(compound)
                target[index] += float(record['volumes'][compound])

            self.inputs.append(self.format_spectrum(record['spectra']))

            self.targets.append(target)

        self.targets = np.array(self.targets)

        # Delete all names that do not exist within the training data
        empty_moities = np.where(~self.targets.any(axis=0))[0]
        self.targets = np.delete(self.targets, empty_moities, axis=1)
        self.compound_names = np.delete(self.compound_names, empty_moities, axis=0).tolist()

    def format_spectrum(self, spectrum):
        spectrum = np.array(spectrum)[100:300]
        return np.concatenate((np.gradient(np.gradient(spectrum)), np.gradient(spectrum), spectrum))

    def generate_model(self):
        self.coefficients = []
        self.errors = []

        for n in range(len(self.inputs[0])):
            xtr = []
            ttr = []

            for f in range(len(self.inputs)):
                xtr.append(self.targets[f])
                ttr.append(self.inputs[f][n])

            xtr = np.array(xtr)
            ttr = np.array(ttr)

            coefs, resid = np.linalg.lstsq(xtr, ttr, rcond=None)[:2]

            self.coefficients.append(coefs)

            if len(resid) == 1:
                self.errors.append(resid[0])
            else:
                self.errors.append(1e-10)

        self.errors = np.array(self.errors)
        self.errors = np.sqrt(self.errors ** 2)

    def generate_spectra(self, x, *argv):
        y = [0] * len(x)

        for i in range(len(x)):
            y[i] = argv @ self.coefficients[i]

        return y

    def predict(self, sample):
        sample = self.format_spectrum(sample)

        x = np.zeros(len(sample))
        y = sample

        popt, pcov = optimize.curve_fit(self.generate_spectra, x, y, p0=[0] * len(self.compound_names), bounds=(0, np.Inf), sigma=self.errors, absolute_sigma=True)

        return popt

    def plot(self, sample, popt):
        sample = self.format_spectrum(sample)

        x = np.zeros(len(sample))
        generated_spectra = self.generate_spectra(x, popt)

        fig, axes = plt.subplots(3, 1)
        plt.tight_layout()

        ax1 = axes[0]
        start = 2 * 200
        end = start + 200
        ax1.set_title("absorption")
        #ax1.plot(errors[start:end], color="k")
        ax2 = ax1.twinx()
        ax2.plot(sample[start:end], color="r")
        ax2.plot(generated_spectra[start:end], color="b")

        ax1 = axes[1]
        start = 1 * 200
        end = start + 200
        ax1.set_title("$1^{st}$ derivative")
        #ax1.plot(errors[start:end], color="k")
        ax2 = ax1.twinx()
        ax2.plot(sample[start:end], color="r")
        ax2.plot(generated_spectra[start:end], color="b")

        ax1 = axes[2]
        start = 0 * 200
        end = start + 200
        ax1.set_title("$2^{nd}$ derivative")
        #ax1.plot(errors[start:end], color="k")
        ax2 = ax1.twinx()
        ax2.plot(sample[start:end], color="r")
        ax2.plot(generated_spectra[start:end], color="b")

        f = io.BytesIO()
        plt.savefig(f, format="png")

        return f

    def label(self, popt):
        moieties = {}

        # Given equation for the water value
        moieties["HOH"] = 2.0 * (float(popt[0]) / 18.0)

        for compound in self.compound_names[1:]:
            for moiety in self.compounds_mapping[compound]["values"]:
                if moiety not in moieties:
                    moieties[moiety] = 0

                moieties[moiety] += (float(self.compounds_mapping[compound]["values"][moiety]) / float(self.compounds_mapping[compound]["rmm"])) * ((float(popt[self.compound_names.index(compound)]) * float(self.compounds_mapping[compound]["density"])) / 1000.0)

        compounds = {}

        for i, compound in enumerate(self.compound_names):
            compounds[compound] = popt[i]

        # Remove insignificant labels
        redundant_names = []
        for name in moieties:
            if moieties[name] < 0.001:
                redundant_names.append(name)

        for name in redundant_names:
            del moieties[name]

        redundant_names = []
        for name in compounds:
            if compounds[name] < 0.001:
                redundant_names.append(name)

        for name in redundant_names:
            del compounds[name]

        return {"substances": compounds, "moieties": moieties}
