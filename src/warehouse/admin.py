from django.contrib import admin, messages
from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import path

import csv
import json

from .models import Predictor, Sample, Record
from .views import UpdateModel


class PredictorAdmin(admin.ModelAdmin):
    list_display = ["name"]

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('<int:pk>/update_model/', self.admin_site.admin_view(UpdateModel.as_view()))
        ]
        return my_urls + urls

    def update_model(self, request, pk):
        messages.error(request, "well done.")
        return redirect(f'/admin/warehouse/predictor/{pk}/')



admin.site.register(Predictor, PredictorAdmin)


class RecordInline(admin.TabularInline):
    model = Record


class SampleAdmin(admin.ModelAdmin):
    list_display = ["reference", "name", "spectrum", "predictor"]

    inlines = [
        RecordInline,
    ]

    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename={self.model._meta}.csv'
        writer = csv.writer(response)

        # All predictor models must be the same to display in csv
        predictor = None
        for obj in queryset:
            if predictor is None:
                predictor = obj.predictor

            if obj.predictor != predictor or obj.predictor is None:
                messages.error(request, "A predictor must be defined, and must be consistent.")
                return redirect('/admin/warehouse/sample/')

        with predictor.moiety_names.open(mode='r') as f:
            moiety_names = json.load(f)

        column_names = ['reference'] + moiety_names

        writer.writerow(column_names)
        for obj in queryset:
            if obj.predictor is None:
                continue

            row = [obj.reference]

            for moiety in column_names[1:]:
                if "moieties" in obj.analysis:
                    if moiety in obj.analysis["moieties"]:
                        row.append(obj.analysis["moieties"][moiety])
                        continue

                row.append(0)

            writer.writerow(row)
        return response

    export_as_csv.short_description = "Export Selected as csv"


admin.site.register(Sample, SampleAdmin)


class RecordAdmin(admin.ModelAdmin):
    pass


admin.site.register(Record, RecordAdmin)
