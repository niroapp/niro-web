from django.conf.urls import url

from channels.routing import ProtocolTypeRouter, URLRouter, ChannelNameRouter

from .middleware import JwtTokenAuthMiddleware

from . import consumers

application = ProtocolTypeRouter({
    'websocket': JwtTokenAuthMiddleware(
        URLRouter([
            url(r'^ws/monitor/$', consumers.MonitorConsumer),
        ])
    ),
    'channel': ChannelNameRouter({"analyser": consumers.AnalysisConsumer}),
})
