from django.contrib.auth.models import Group
from rest_framework import serializers

from .models import Sample, Record


class SampleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sample
        fields = ('owner', 'reference', 'name', 'analysis', 'predictor', 'spectrum')


class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Record
        fields = ('sample', 'spectrum')


class ReadAnalyseSerializer(serializers.Serializer):
    spectrum = serializers.ListField(child=serializers.FloatField())


class SampleAnalyseSerializer(serializers.Serializer):
    reference = serializers.CharField(required=True, max_length=36)
    name = serializers.CharField(required=True, max_length=36)
    predictor = serializers.IntegerField(required=False)
    reads = ReadAnalyseSerializer(many=True)


class AnalyseSerializer(serializers.Serializer):
    samples = SampleAnalyseSerializer(many=True)
