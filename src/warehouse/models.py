from django.contrib.postgres.fields import ArrayField, JSONField
from django.db import models
from django.conf import settings


class Predictor(models.Model):
    name = models.CharField(max_length=36)
    moiety_names = models.FileField(upload_to='moiety_names', blank=True, null=True)
    compounds_mapping = models.FileField(upload_to='compounds_mapping', blank=True, null=True)
    training_data = models.FileField(upload_to='training_data', blank=True, null=True)

    def __str__(self):
        return self.name


class Sample(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    reference = models.CharField(max_length=36)
    name = models.CharField(max_length=36, default="")
    analysis = JSONField(default=dict)
    predictor = models.ForeignKey(Predictor, on_delete=models.SET_NULL, blank=True, null=True)

    spectrum = models.ImageField(upload_to='spectra', blank=True, null=True)

    def __str__(self):
        return self.reference


class Record(models.Model):
    sample = models.ForeignKey('warehouse.Sample', on_delete=models.CASCADE)
    spectrum = ArrayField(models.FloatField(), size=331)
