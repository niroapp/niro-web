{
  "samples": [
    {
      "reference": "1",
      "reads": [
        {
          "spectrum": [1, 2, 3]
        },
        {
          "spectrum": [1, 2, 3]
        },
        {
          "spectrum": [1, 2, 3]
        }
      ]
    },
    {
      "reference": "1",
      "reads": [
        {
          "spectrum": [1, 2, 3]
        },
        {
          "spectrum": [1, 2, 3]
        },
        {
          "spectrum": [1, 2, 3]
        }
      ]
    }
  ]
}


curl -d '{"samples":[]}' \
-H "Content-Type: application/json" \
-H "Authorization: Token 7a725fc0586951a81a947b0a8cd3d86afb80efc5" \
-X POST http://192.168.99.100:8000/api/samples/analyse/

curl -d '{"samples":[]}' \
-H "Content-Type: application/json" \
-H "Authorization: Token 3f7e075fd452759f2e3d4b4debeb0727a3108931" \
-X POST http://176.58.124.102:8000/api/samples/analyse/
