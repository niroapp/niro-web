FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /src
RUN mkdir /static
WORKDIR /src
ADD ./src /src
RUN pip install --upgrade pip
RUN pip install -r requirements.pip
CMD python manage.py collectstatic --no-input;python manage.py migrate; daphne -b 0.0.0.0 -p 8000 ecmm427.asgi:application & python manage.py runworker analyser
