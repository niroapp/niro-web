# Install

This has been setup on a cloud server running Debian 9.

Add a non-root user:
```
adduser william
adduser william sudo
nano /etc/ssh/sshd_config
>>> PermitRootLogin no
```

Install Docket:
```
sudo apt update
sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install docker-ce
sudo systemctl status docker
sudo usermod -aG docker ${USER}
```

Install Docker Compose:
```
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
```

Install project:
```
cd /home
sudo mkdir ECMM427-web
sudo chown william:docker ECMM427-web
git clone https://github.com/WilliamWCYoung/ECMM427-web.git
cd ./ECMM427-web
make build
make up
make start
```
